#  MiniGolf

First attempt to create a native iOS app based on swift. Everytning is baed on lists and navigation views

This is an alternative to the paper scoreboard handed out on every miniature golf course.

## Features

* Create players
* Create a game with X holes and Y players
* Every game gets a GPS coordinate and a timestamp


## References

* Using glyphs from [https://icons8.com](https://icons8.com/icon/set/info/ios-glyphs)
