//
//  Hole.swift
//  MiniGolf
//
//  Created by Rune Memborg on 27/07/2018.
//  Copyright © 2018 dk.superrune. All rights reserved.
//

import Foundation
import CoreData

public class Hole: NSManagedObject {
    
    var name: String {
        get {
            if self.number == 0 {
                return "Total"
            }
            
            return "Hul \(self.number)"
        }
    }
}
