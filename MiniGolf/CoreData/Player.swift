//
//  Player.swift
//  MiniGolf
//
//  Created by Rune Memborg on 05/07/2018.
//  Copyright © 2018 dk.superrune. All rights reserved.
//

import UIKit
import CoreData

public class Player: NSManagedObject {
    
    var age: Int {
        get {
            let date = Date();
            let calendar = Calendar.current
            
            let year = calendar.component(.year, from: date)
            return year - Int(self.birthYear)
        }
    }

}
