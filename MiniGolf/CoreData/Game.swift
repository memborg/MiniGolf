//
//  Game.swift
//  MiniGolf
//
//  Created by Rune Memborg on 28/07/2018.
//  Copyright © 2018 dk.superrune. All rights reserved.
//

import Foundation
import CoreData

public class Game: NSManagedObject {
    var dateFormat = "dd-MM-yyyy"
    var humanDate: String? {
        get {
            return created?.toString(dateFormat: dateFormat)
        }
    }
    
}
