//
//  DataSeeder.swift
//  MiniGolf
//
//  Created by Rune Memborg on 08/07/2018.
//  Copyright © 2018 dk.superrune. All rights reserved.
//

import Foundation
import CoreData

public class DataHelper {
    let context: NSManagedObjectContext
    
    init(context: NSManagedObjectContext) {
        self.context = context
    }
    
    public func seedDataStore() {
        clearGames()
//        clearPlayers()
//        seedPlayers()
    }
    
    private func clearGames() {
        let fetch = NSFetchRequest<NSFetchRequestResult>(entityName: "Game")
        let request = NSBatchDeleteRequest(fetchRequest: fetch)
        
        _ = try? context.execute(request)
    }
    
    private func clearPlayers() {
        let fetch = NSFetchRequest<NSFetchRequestResult>(entityName: "Player")
        let request = NSBatchDeleteRequest(fetchRequest: fetch)
        
        _ = try? context.execute(request)
    }
    
    private func seedPlayers() {
        let players = [
            (name: "Rasmus", birthYear: 2009),
            (name: "Kathrine", birthYear: 2007),
            (name: "Louise", birthYear: 1984),
            (name: "Rune", birthYear: 1981)
        ]
        
        for player in players {
            let newPlayer = NSEntityDescription.insertNewObject(forEntityName: "Player", into: context) as! Player
            newPlayer.name = player.name
            newPlayer.birthYear = Int32(player.birthYear)
        }
        
        do {
            try context.save()
        } catch _ {
        }
    }
}
