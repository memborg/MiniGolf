//
//  PlayersTVControllerTableViewController.swift
//  MiniGolf
//
//  Created by Rune Memborg on 01/07/2018.
//  Copyright © 2018 dk.superrune. All rights reserved.
//

import UIKit
import CoreData

class ChoosePlayerTVController: FetchedResultsTableViewController {
    
    var players = ChoosePlayerData()
    
    var fetchedResultsController: NSFetchedResultsController<Player>?
    
    var context = AppDelegate.viewContext
    
    override func viewDidLoad() {
        super.viewDidLoad()
        updateUI()
    }
    
    private func updateUI() {
        let request: NSFetchRequest<Player> = Player.fetchRequest()
        request.sortDescriptors = [NSSortDescriptor(
            key: "name",
            ascending: true,
            selector: #selector(NSString.localizedCaseInsensitiveCompare(_:))
            )]
        fetchedResultsController = NSFetchedResultsController<Player>(
            fetchRequest: request,
            managedObjectContext: context,
            sectionNameKeyPath: nil,
            cacheName: nil
        )
        fetchedResultsController?.delegate = self
        try? fetchedResultsController?.performFetch()
        tableView.reloadData()
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return fetchedResultsController?.sections?.count ?? 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let sections = fetchedResultsController?.sections , sections.count > 0 {
            return sections[section].numberOfObjects
        }
        
        return 0
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "PlayerCell", for: indexPath)
        
        if let obj = fetchedResultsController?.object(at: indexPath) {
            cell.textLabel?.text = obj.name
            cell.detailTextLabel?.text = "\(obj.age) år"
            cell.accessoryType = .none
            
            if players.players.contains(obj) {
                cell.accessoryType = .checkmark
            }
            
        }
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath)!
        
        if let obj = fetchedResultsController?.object(at: indexPath) {
            players.players.append(obj)
            cell.accessoryType = .checkmark
        }
    }
    
    override func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath)
        
        if let obj = fetchedResultsController?.object(at: indexPath), let index = players.players.index(of: obj) {
            players.players.remove(at: index)
            cell?.accessoryType = .none
        }
    }
}
