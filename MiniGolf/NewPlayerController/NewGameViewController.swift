//
//  NewGameViewController.swift
//  MiniGolf
//
//  Created by Rune Memborg on 17/07/2018.
//  Copyright © 2018 dk.superrune. All rights reserved.
//

import UIKit
import CoreData
import CoreLocation

class NewGameViewController: UIViewController, UITextFieldDelegate, UITableViewDelegate, UITableViewDataSource, CLLocationManagerDelegate {
    
    var players = ChoosePlayerData()
    
    let locationManager = CLLocationManager()
    var location: CLLocation?
    
    var context = AppDelegate.viewContext
    override func viewDidLoad() {
        super.viewDidLoad()
        
        loadUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        players.players.propertySortAsc({$0.name!})
        
        tableView.reloadData()
    }
    
    private func loadUI() {
        NumberOfHoles.keyboardType = UIKeyboardType.numberPad
        NumberOfHoles.text = "18"
        
        // For use when the app is open
        locationManager.requestWhenInUseAuthorization()
        
        // If location services is enabled get the users location
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyBest // You can change the locaiton accuary here.
            locationManager.startUpdatingLocation()
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let location = locations.first {
            self.location = location
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if(status == CLAuthorizationStatus.denied) {
            showLocationDisabledPopUp()
        }
    }
    
    func showLocationDisabledPopUp() {
        let alertController = UIAlertController(title: "Background Location Access Disabled",
                                                message: "In order to deliver pizza we need your location",
                                                preferredStyle: .alert)
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        alertController.addAction(cancelAction)
        
        let openAction = UIAlertAction(title: "Open Settings", style: .default) { (action) in
            if let url = URL(string: UIApplicationOpenSettingsURLString) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            }
        }
        alertController.addAction(openAction)
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    private func numberOfHoles() -> Int {
        if let holes = NumberOfHoles?.text {
            return Int(holes) ?? 0
        }
        
        return 0
    }
    
    @IBOutlet weak var tableView: UITableView! {
        didSet {
            tableView.delegate = self
        }
    }
    
    @IBOutlet weak var NumberOfHoles: UITextField! {
        didSet {
            NumberOfHoles.delegate = self
        }
    }
    
    //     MARK: - Navigation
    
    //     In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if let identifier = segue.identifier {
            switch identifier {
            case "CreateAndGotoGame":
                if players.players.count > 0, numberOfHoles() > 0{
                    let game = createGame()
                    if let seguedToMVC = segue.destination as? GameTVController {
                        seguedToMVC.game = (context.object(with: game.objectID) as! Game)
                    }
                } else if players.players.count < 1{
                    missingPlayerAlert()
                } else if numberOfHoles() < 1 {
                    missingHoles()
                }
            case "ChoosePlayer":
                if let segueToMVC = segue.destination as? ChoosePlayerTVController {
                    players.players = []
                    segueToMVC.players = players
                }
            default: break
            }
        }
    }
    
    private func showAlert(title: String, message: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
            switch action.style{
            case .default:
                print("default")
                
            case .cancel:
                print("cancel")
                
            case .destructive:
                print("destructive")
                
                
            }}))
        self.present(alert, animated: true, completion: nil)
    }
    
    private func missingHoles() {
        showAlert(title: "Ingen huller?", message: "Du skal huske, at angive antallet af huller på banen.")
    }
    
    private func missingPlayerAlert() {
        showAlert(title: "Ingen spillere?", message: "Du skal vælge spillere ind vi kan starte spillet")
    }
    
    private func createGame() -> Game{
        let game = NSEntityDescription.insertNewObject(forEntityName: "Game", into: context) as! Game
        
        let holes = numberOfHoles() + 1
        
        game.created = Date()
        game.finished = false
        
        if let coordinates = location?.coordinate {
            game.latitude = String(coordinates.latitude)
            game.longitude = String(coordinates.longitude)
        }
        
        for hole in 0..<holes {
            let newHole = NSEntityDescription.insertNewObject(forEntityName: "Hole", into: context) as! Hole
            newHole.number = Int16(hole)
            newHole.game = game
            newHole.finished = false
            
            for player in players.players {
                let newScore = NSEntityDescription.insertNewObject(forEntityName: "Score", into: context) as! Score
                newScore.hole = newHole
                newScore.stroke = 0
                newScore.player = player
                newScore.sort = 0
            }
        }
        
        try! context.save()
        locationManager.stopUpdatingLocation()
        
        return game
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        textField.resignFirstResponder()
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        textField.becomeFirstResponder()
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return "Spillere"
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return players.players.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "AddedPlayerCell", for: indexPath)
        
        cell.textLabel?.text = players.players[indexPath.row].name
        
        return cell
    }
}
