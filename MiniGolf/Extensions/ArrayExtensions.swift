//
//  ArrayExtensions.swift
//  MiniGolf
//
//  Created by Rune Memborg on 19/07/2018.
//  Copyright © 2018 dk.superrune. All rights reserved.
//

extension Array {
    mutating func propertySortAsc<T: Comparable>(_ property: (Element) -> T) {
        sort(by: { property($0) < property($1) })
    }
    
    mutating func propertySortDesc<T: Comparable>(_ property: (Element) -> T) {
        sort(by: { property($0) > property($1) })
    }
}
