//
//  DateFormatter.swift
//  MiniGolf
//
//  Created by Rune Memborg on 17/07/2018.
//  Copyright © 2018 dk.superrune. All rights reserved.
//

import Foundation

extension Date
{
    func toString( dateFormat format  : String ) -> String
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        return dateFormatter.string(from: self)
    }
}
