//
//  PlayerDetailControllerViewController.swift
//  MiniGolf
//
//  Created by Rune Memborg on 01/07/2018.
//  Copyright © 2018 dk.superrune. All rights reserved.
//

import UIKit
import CoreData

class PlayerDetailViewController: UIViewController, UITextFieldDelegate {
    
    var playerDetail: Player? = nil
    var context = AppDelegate.viewContext
    
    var player: Player? {
        didSet {
            playerDetail = player
            updatePlayer()
        }
    }
    
    @IBOutlet weak var PlayerName: UITextField! {
        didSet{
            PlayerName.delegate = self
        }
    }
    @IBOutlet weak var PlayerBirthYear: UITextField! {
        didSet {
            PlayerBirthYear.delegate = self
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        PlayerBirthYear.keyboardType = UIKeyboardType.numberPad
        PlayerName.autocapitalizationType = UITextAutocapitalizationType.init(rawValue: 1)!
        updatePlayer()
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        textField.resignFirstResponder()
        savePlayer()
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        textField.becomeFirstResponder()
    }
    
    private func savePlayer() {
        if let objectID = playerDetail?.objectID {
            let player = context.object(with: objectID) as! Player
            
            player.name = PlayerName?.text
            player.birthYear = Int32(birthYear())
            
        } else {
            let player = NSEntityDescription.insertNewObject(forEntityName: "Player", into: context) as! Player
            player.name = PlayerName?.text
            player.birthYear = Int32(birthYear())
            
            self.player = player;
        }
        
        try? context.save()
    }
    
    private func birthYear() -> Int {
        if let birthYear = (PlayerBirthYear?.text) {
            return Int(birthYear) ?? 0
        }
        
        return 0
    }
    
    private func updatePlayer(){
        if PlayerName != nil {
            PlayerName?.text = playerDetail?.name
            if let birthYear = playerDetail?.birthYear {
                PlayerBirthYear?.text = String(birthYear)
            }
        }
    }
}
