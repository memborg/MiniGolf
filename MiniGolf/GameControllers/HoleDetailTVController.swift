//
//  HoleDetailTVController.swift
//  MiniGolf
//
//  Created by Rune Memborg on 18/07/2018.
//  Copyright © 2018 dk.superrune. All rights reserved.
//

import UIKit

class HoleDetailTVController: UITableViewController {

    private var holeDetail: Hole? = nil
    var context = AppDelegate.viewContext
    var scores = [Score]()
    
    var hole: Hole? {
        didSet {
            holeDetail = hole
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = holeDetail?.name
        
        scores = holeDetail?.scores?.allObjects as! [Score]
        
        if scores[0].sort == 0 {
            scores.propertySortAsc({$0.player!.age})
        } else {
            scores.propertySortAsc({$0.sort})
        }
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return scores.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "HoleCell", for: indexPath) as! ScoreTVCell

        let row = indexPath.row
        let score = scores[row]
        
        cell.PlayerName.text = score.player?.name
        cell.ScoreInput.text = String(score.stroke)
        cell.hole = holeDetail
        cell.score = score

        return cell
    }
}
