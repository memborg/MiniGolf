//
//  GamesMapViewController.swift
//  MiniGolf
//
//  Created by Rune Memborg on 28/07/2018.
//  Copyright © 2018 dk.superrune. All rights reserved.
//

import UIKit
import CoreData
import MapKit

class GamesMapViewController: UIViewController, NSFetchedResultsControllerDelegate, MKMapViewDelegate {
    
    @IBOutlet var GamesMap: MKMapView!{
        didSet {
            GamesMap.delegate = self
        }
    }
    
    var fetchedResultsController: NSFetchedResultsController<Game>?
    var context = AppDelegate.viewContext
    let regionRadius: CLLocationDistance = 5000
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initMap()
        loadMap()
    }
    
    public func controllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        loadMap()
    }
    
    public func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange sectionInfo: NSFetchedResultsSectionInfo, atSectionIndex sectionIndex: Int, for type: NSFetchedResultsChangeType) {
        switch type {
        case .insert: loadMap()
        case .delete: loadMap()
        default: break
        }
    }
    
    public func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {
        switch type {
        case .insert:
            loadMap()
        case .delete:
            loadMap()
        case .update:
            loadMap()
        case .move:
            loadMap()
        }
    }
    
    public func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        loadMap()
    }
    
    private func initMap() {
        GamesMap.mapType = .standard
        GamesMap.isZoomEnabled = true
        GamesMap.isScrollEnabled = true
        
        //Start the map in Aalborg, Denmark
        let initialLocation = CLLocation(latitude: 57.0480000, longitude: 9.9187000 )
        centerMapOnLocation(location: initialLocation)
    }
    
    private func loadMap() {
        let request: NSFetchRequest<Game> = Game.fetchRequest()
        request.sortDescriptors = [NSSortDescriptor(
            key: "created",
            ascending: false,
            selector: #selector(NSString.localizedCaseInsensitiveCompare(_:))
            )]
        
        fetchedResultsController = NSFetchedResultsController<Game>(
            fetchRequest: request,
            managedObjectContext: context,
            sectionNameKeyPath: nil,
            cacheName: nil
        )
        
        fetchedResultsController?.delegate = self
        try? fetchedResultsController?.performFetch()
        
        let games = fetchedResultsController?.fetchedObjects
        
        for game in games! {
            
            if let longitude = game.longitude, let latitude = game.latitude{
                let coor = CLLocationCoordinate2D(latitude: Double(latitude)!, longitude: Double(longitude)!)
                
                let name = game.humanDate
                
                let pin = GamePin(title: name!, locationName: name!, coordinate: coor)
                
                GamesMap.addAnnotation(pin)
            }
        }
    }
    
    private func centerMapOnLocation(location: CLLocation) {
        let coordinateRegion = MKCoordinateRegionMakeWithDistance(location.coordinate, regionRadius, regionRadius)
        GamesMap.setRegion(coordinateRegion, animated: true)
    }
}
