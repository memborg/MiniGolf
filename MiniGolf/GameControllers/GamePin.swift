//
//  GamePin.swift
//  MiniGolf
//
//  Created by Rune Memborg on 27/07/2018.
//  Copyright © 2018 dk.superrune. All rights reserved.
//

import Foundation
import MapKit

class GamePin: NSObject, MKAnnotation {
    let title: String?
    let locationName: String
    let coordinate: CLLocationCoordinate2D
    
    init(title: String, locationName: String, coordinate: CLLocationCoordinate2D) {
        self.title = title
        self.locationName = locationName
        self.coordinate = coordinate
        
        super.init()
    }
    
    var subtitle: String? {
        return locationName
    }
}
