//
//  GameDetailViewController.swift
//  MiniGolf
//
//  Created by Rune Memborg on 27/07/2018.
//  Copyright © 2018 dk.superrune. All rights reserved.
//

import UIKit
import MapKit

class GameDetailViewController: UIViewController, MKMapViewDelegate{
    
    let dateFormat = "dd-MM-yyyy"
    var rawDate = Date()
    private var gameDetail: Game?
    var context = AppDelegate.viewContext
    let datePicker:UIDatePicker = UIDatePicker()
    
    let regionRadius: CLLocationDistance = 1000
    
    var game: Game? {
        didSet {
            gameDetail = game
        }
    }

    @IBOutlet weak var GameDateField: UITextField!
    
    @IBOutlet weak var GameMap: MKMapView! {
        didSet {
            GameMap.delegate = self
        }
    }
    
    @IBOutlet weak var NumberOfHoles: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if let numberOfHoles = gameDetail?.holes?.count {
            NumberOfHoles.text = String(numberOfHoles - 1)
        }
        
        GameDateField.text = gameDetail?.humanDate
        
        self.title = gameDetail?.humanDate
        
        datePicker.datePickerMode = UIDatePickerMode.date
        datePicker.locale = Locale(identifier: "da-DK")
        GameDateField.inputView = datePicker
        
        //ToolBar
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        let doneButton = UIBarButtonItem(title: "Færdig", style: .plain, target: self, action: #selector(donedatePicker))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Annuller", style: .plain, target: self, action: #selector(cancelDatePicker))
        
        toolbar.setItems([cancelButton,spaceButton,doneButton], animated: true)
        
        GameDateField.inputAccessoryView = toolbar
        GameDateField.inputView = datePicker
        
        datePicker.date = gameDetail!.created!
        
        GameMap.mapType = .standard
        GameMap.isZoomEnabled = false
        GameMap.isScrollEnabled = false
        

        
        if let longitude = gameDetail!.longitude, let latitude = gameDetail!.latitude{
            let initialLocation = CLLocation(latitude: Double(latitude)!, longitude: Double(longitude)! )
            centerMapOnLocation(location: initialLocation)
            
            let coor = CLLocationCoordinate2D(latitude: Double(latitude)!, longitude: Double(longitude)!)
            
            let name = gameDetail?.humanDate
            
            let pin = GamePin(title: name!, locationName: name!, coordinate: coor)
            
            GameMap.addAnnotation(pin)
        }
    }
    
    @objc func donedatePicker(){
        let dateFormatter = formatter()
        rawDate = datePicker.date
        GameDateField.text = dateFormatter.string(from: datePicker.date)
        self.view.endEditing(true)
        updateGame()
    }
    
    @objc func cancelDatePicker(){
        self.view.endEditing(true)
    }
    
    private func formatter() -> DateFormatter {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = gameDetail?.dateFormat
        
        return dateFormatter
    }
    
    private func updateGame() {
        let objectId = gameDetail!.objectID
        let game = context.object(with: objectId) as! Game
        game.created = rawDate
        
        try! context.save()
        
        self.title = gameDetail?.humanDate
    }
    
    private func centerMapOnLocation(location: CLLocation) {
        let coordinateRegion = MKCoordinateRegionMakeWithDistance(location.coordinate, regionRadius, regionRadius)
        GameMap.setRegion(coordinateRegion, animated: true)
    }
}
