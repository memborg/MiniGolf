//
//  GameTVController.swift
//  MiniGolf
//
//  Created by Rune Memborg on 05/07/2018.
//  Copyright © 2018 dk.superrune. All rights reserved.
//

import UIKit
import CoreData

class GameTVController: FetchedResultsTableViewController {
    
    var fetchedResultsController: NSFetchedResultsController<Hole>?
    var gameDetail: Game? = nil
    var context = AppDelegate.viewContext
    
    var game: Game? {
        didSet {
            gameDetail = game
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        updateUI()
        prepareTotal()
    }
    
    private func prepareTotal() {
        var holes = fetchedResultsController?.fetchedObjects
        
        let totalHole = holes![0]
        var totalScores = totalHole.scores?.allObjects as! [Score]
        
        for score in totalScores {
            score.stroke = 0
        }
        
        // Find total per player and update stroke count
        for index in 1..<holes!.count {
            let hole = holes![index]
            let scores = hole.scores?.allObjects as! [Score]
            
            for score in scores {
                let foundIndex = findTotalScoreIndex(player: score.player!, inScores: totalScores)
                if foundIndex > -1 {
                    totalScores[foundIndex].stroke += score.stroke
                }
            }
        }
        
        // Save the updated total
        for total in totalScores {
            let objectId = total.objectID
            let score = context.object(with: objectId) as! Score
            score.stroke = Int16(score.stroke)
            
            try? context.save()
        }
        
        // Update player sort order on all holes
        for index in 0..<holes!.count {
            let hole = holes![index]
            let scores = hole.scores?.allObjects as! [Score]
            
            for score in scores {
                let foundIndex = findTotalScoreIndex(player: score.player!, inScores: totalScores)
                if foundIndex > -1 {
                    let objectId = score.objectID
                    let updatedScore = context.object(with: objectId) as! Score
                    updatedScore.sort = totalScores[foundIndex].stroke
                    
                    try? context.save()
                }
            }
        }
        
    }
    
    private func findTotalScoreIndex(player: Player, inScores: [Score]) -> Int {
        for index in 0..<inScores.count {
            let score = inScores[index]
            if score.player?.name == player.name {
                return index
            }
        }
        
        return -1
    }
    
    private func updateUI() {
        
        self.title = gameDetail?.humanDate
        
        let created = gameDetail!.created! as NSDate
        
        let request: NSFetchRequest<Hole> = Hole.fetchRequest()
        request.sortDescriptors = [NSSortDescriptor(
            key: "number",
            ascending: true,
            selector: #selector(NSString.localizedCaseInsensitiveCompare(_:))
            )]
        request.predicate = NSPredicate(format: "game.created == %@", created)
        fetchedResultsController = NSFetchedResultsController<Hole>(
            fetchRequest: request,
            managedObjectContext: context,
            sectionNameKeyPath: nil,
            cacheName: nil
        )
        fetchedResultsController?.delegate = self
        try? fetchedResultsController?.performFetch()
        tableView.reloadData()
    }
    
    private func getHoleWinner(scores: [Score]) -> Score {
        return scores.min(by: {$0.stroke < $1.stroke})!
    }
    
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let sectionsFRC = fetchedResultsController?.sections , sectionsFRC.count > 0{
            return sectionsFRC[0].numberOfObjects
        }
        
        return 0
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "HoleCell", for: indexPath)
        
        if let obj = fetchedResultsController?.object(at: indexPath) {
            cell.textLabel?.text = obj.name
            
            let scores = obj.scores?.allObjects as! [Score]
            let winner = getHoleWinner(scores: scores.sorted(by: {$0.player!.name! < $1.player!.name!}))
            
            if obj.number == 0 {
                cell.detailTextLabel?.text = "\(winner.player!.name!) vandt i \(winner.stroke) slag"
            } else if winner.stroke < 1 {
                cell.detailTextLabel?.text = "Hul ikke spillet endnu"
            } else {
                cell.detailTextLabel?.text = "\(winner.player!.name!) vandt i \(winner.stroke) slag"
            }
        }
        
        return cell
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let identifier = segue.identifier {
            switch identifier {
            case "HoleDetail":
                if let cell = sender as? UITableViewCell,
                    let indexPath = tableView.indexPath(for: cell),
                    let seguedToMVC = segue.destination as? HoleDetailTVController {
                    if let obj = fetchedResultsController?.object(at: indexPath) {
                        seguedToMVC.hole = obj
                    }
                }
            case "ShowGameDetail":
                if let seguedToMVC = segue.destination as? GameDetailViewController {
                    seguedToMVC.game = gameDetail
                }
            default: break
            }
        }
    }
}
