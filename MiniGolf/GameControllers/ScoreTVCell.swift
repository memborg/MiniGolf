//
//  ScoreTVCell.swift
//  MiniGolf
//
//  Created by Rune Memborg on 19/07/2018.
//  Copyright © 2018 dk.superrune. All rights reserved.
//

import UIKit
import CoreData

class ScoreTVCell: UITableViewCell, UITextFieldDelegate {

    @IBOutlet weak var PlayerName: UILabel!
    @IBOutlet weak var ScoreInput: UITextField! {
        didSet {
            ScoreInput.delegate = self
        }
    }
    
    private var scoreInfo: Score?
    private var holeInfo: Hole?
    var context = AppDelegate.viewContext
    
    var score: Score? {
        didSet {
            scoreInfo = score
        }
    }
    var hole: Hole? {
        didSet {
            holeInfo = hole
        }
    }
    
    private func saveScore() {
        let objectId = scoreInfo!.objectID
        let score = context.object(with: objectId) as! Score
        score.stroke = Int16(getScore())
        
        try? context.save()
    }
    
    private func getScore() -> Int {
        if let s = ScoreInput.text {
            return Int(s) ?? 0
        }
        return 0
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if holeInfo?.number == 0 {
            return false
        }
        
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        textField.resignFirstResponder()
        saveScore()
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        textField.becomeFirstResponder()
    }

}
